import { greeting } from '.';

it('should be return greeting message', () => {
    expect(greeting()).toBe('Hello, world');
});
