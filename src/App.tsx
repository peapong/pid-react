import React, { MouseEventHandler, useState } from 'react';
import styles from './App.module.css';
type IAppPage = () => React.ReactElement;

const App: IAppPage = () => {
    const [inputId, setInputId] = useState('');
    const [isOpen, setIsOpen] = useState(false);
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        console.log(e.target.value);
        setInputId(e.target.value);
    };
    const checkID = () => {
        const li: HTMLElement = document.createElement('li');
        const text = document.createTextNode(inputId);
        li.appendChild(text);
        if (inputId === '') {
            alert('โปรดกรอกเลขบัตรประชาชน');
            return;
        }
        let sum = 0;
        let i;
        for (i = 0, sum = 0; i < 12; i++) {
            sum += parseFloat(inputId.charAt(i)) * (13 - i);
        }
        const mod = sum % 11;
        const idCheck = (11 - mod) % 10;
        if (idCheck === parseInt(inputId.charAt(12))) {
            alert('หมายเลขบัตรประชาชนถูกต้อง');
            setIsOpen(true);
            return true;
        }
        if (idCheck !== parseInt(inputId.charAt(12))) {
            alert('หมายเลขบัตรประชาชนผิด โปรดกรอกใหม่อีกครั้ง');
            setIsOpen(false);
            return false;
        }
    };
    return (
        <div>
            <h1>PID Check</h1>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <input
                    type="text"
                    name="name"
                    className={styles.Text}
                    value={inputId}
                    onChange={handleChange}
                    maxLength={13}
                    // onClick={() => setIsOpen((prev) => !prev)}
                />
            </div>
            <br></br>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <button onClick={checkID}>Check</button>
            </div>
            <p>Your ID : {inputId}</p>
            {isOpen ? <p>✅</p> : <p>❌</p>}
            {/* {isOpen && <p>✅</p>} */}
            {/* <button onClick={() => setIsOpen((prev) => !prev)}>Check</button> */}
            <div>
                <ul id="listUL">
                    <li>1234567890121</li>
                    {/* <li></li> */}
                </ul>
            </div>
        </div>
    );
};
export default App;
